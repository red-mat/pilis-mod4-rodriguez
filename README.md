# TicketSwift

Es un sistema backend de reserva de tickets

# Deploy

- Antes que nada es necesario tener node instalado en el sistema.
- Se puede usar npm pero es recomendable usar pnpm
- Antes de seguir los pasos correspondientes instale las dependencias:
  - pnpm:
    ```sh
    pnpm i
    ```
  - npm:
    ```sh
    npm i
    ```

## scripts

- **npm start**: transipila e interpreta el código de producción
- **npm run lint**: trata de limpiar y resolver fixes en el código
- **npm run format**: formatea el código

## Deploy

1. **Configurar las variables de entorno:** Las variables de entorno se encuentran en la carpeta .env
2. **Ejecutar la base de datos con docker compose:** Se carga las variables de entorno de la base de datos.

   ```sh
   docker compose --env-file .env/db.env up -d
   ```

3. **Despliegue**
   ```sh
   npm start
   ```

# [Guía](./docs/assets/guide.pdf) de desarrollo

## Entorno de Desarrollo

- arquitectura: Screaming Architecture & clean architecture
- lenguaje: typescript
- framework: express
- formatter: prettier
- linter: eslint standard
- dev runner: ts-node-dev
- api doc: Swagger

# Modelo de la base de datos

![db-model](./docs/assets/db.svg)

# Casos de usos

![use-case](./docs/assets/usecase.svg)

## Tareas

- [x] Generar el entorno de desarrollo
- [x] Generar diagrama de BD
- [x] Generar diagrama de casos de usos
- [x] Generar el sistema de usuarios
- [x] Generar el sistema de eventos
- [x] Generar el sistema de reservaciones (bookings)
