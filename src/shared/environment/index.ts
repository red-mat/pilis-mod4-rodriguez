import dotenv from 'dotenv'
import type Data from './data'

dotenv.config({ path: '.env/globals.env' })
dotenv.config({ path: '.env/db.env' })

export default process.env as unknown as Data
