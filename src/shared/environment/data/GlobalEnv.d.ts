export default interface GlobalsEnv {
  PORT: string
  DOMAIN: string
  ROOT_PATH: string
}
