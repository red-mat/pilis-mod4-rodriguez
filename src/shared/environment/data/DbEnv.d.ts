export default interface DbEnv {
  DB_NAME: string
  DB_PASSWORD: string
  DB_PORT: string
  DB_HOST: string
}
