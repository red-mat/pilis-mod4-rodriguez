import type DbEnv from './DbEnv'
import type GlobalEnv from './GlobalEnv'

export default interface Data extends GlobalEnv, DbEnv {}
