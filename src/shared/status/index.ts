export interface Status<T> {
  code: number
  isError: boolean
  message?: string
  payload?: T
}
