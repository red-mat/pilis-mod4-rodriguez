interface Information {
  pages: number
  count: number
  next: string | null
  prev: string | null
}

export default interface PaginatedResponse<T> {
  info: Information
  result: T[]
}
