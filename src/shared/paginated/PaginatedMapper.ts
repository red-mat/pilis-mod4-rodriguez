import type { PaginatedDto } from './Paginated'
import type PaginatedResponse from './PaginatedResponse'

export default class PaginatedMapper {
  constructor(private readonly url: string) {}

  private _getUri(page: number): string {
    const url = this.url
    const uri = `${url}/?page=${page}`

    return uri
  }

  private _nextPage(page: number, limit: number): string | null {
    if (page >= limit) return null
    return this._getUri(page + 1)
  }

  private _prevPage(page: number): string | null {
    if (page === 1) return null
    return this._getUri(page - 1)
  }

  toPaginatedResponse<T>(data: PaginatedDto<T>): PaginatedResponse<T> {
    const { count, list, page, pages } = data

    const next = this._nextPage(page, pages)
    const prev = this._prevPage(page)

    return {
      info: {
        count,
        pages,
        next,
        prev,
      },
      result: list,
    }
  }
}
