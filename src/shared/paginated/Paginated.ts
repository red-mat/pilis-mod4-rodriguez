export interface PaginatedDto<T> {
  page: number
  pages: number
  count: number
  list: T[]
}
