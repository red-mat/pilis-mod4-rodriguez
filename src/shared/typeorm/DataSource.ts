import { DataSource } from 'typeorm'
import environment from '../environment'
import UserSchema from '@/Users/infrastructure/repositories/UserMySqlRepository/schema'
import {
  EventsSchema,
  GpsSchema,
} from '@/Events/infrastructure/EventsMySqlRepository/schema'
import { BookingSchema } from '@/Bookings/entity'

const { DB_NAME, DB_HOST, DB_PASSWORD, DB_PORT } = environment

export default new DataSource({
  type: 'mysql',
  host: DB_HOST,
  port: Number(DB_PORT),
  username: 'root',
  password: DB_PASSWORD,
  database: DB_NAME,
  synchronize: true,
  entities: [UserSchema, EventsSchema, GpsSchema, BookingSchema],
})
