import type { PaginatedDto } from '../paginated'
import type { Status } from '../status'

export interface SearchStatus<T> extends Status<PaginatedDto<T>> {}
