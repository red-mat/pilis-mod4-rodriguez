import type { Response, ResponseSearch } from './Response'

export default abstract class Repository<TR, TE, TId> {
  abstract add(e: TE): Response
  abstract del(id: TId): Response
  abstract update(e: TR): Response
  abstract search(e: Partial<TR>, page?: number): ResponseSearch<TR>
}
