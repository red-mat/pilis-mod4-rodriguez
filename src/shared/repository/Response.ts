import type { SearchStatus } from './status'
import type { Status } from '../status'

export type Response = Promise<Status<undefined>>
export type ResponseSearch<T> = Promise<SearchStatus<T>>
