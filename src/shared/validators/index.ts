export const isDefined = (x: any): boolean => x != null
export const isInstance = (O: any, T: any): boolean => O instanceof T

export const isString = (str: string): boolean => typeof str === 'string'
export const isNumber = (num: number): boolean => typeof num === 'number'
export const isInt = (num: number): boolean =>
  isNumber(num) && Number.isInteger(num)
export const isFloat = (num: number): boolean =>
  isNumber(num) && !Number.isInteger(num)
