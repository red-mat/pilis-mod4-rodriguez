import { type Request, type Response } from 'express'

export abstract class Controller {
  constructor(readonly path: string) {
    this.handler = this.handler.bind(this)
  }

  abstract handler(req: Request, res: Response): Promise<void> | void

  withParam(param: string): string {
    return `${this.path}/:${param}`
  }

  compose(path: string): string {
    return `${this.path}/:${path}`
  }
}
