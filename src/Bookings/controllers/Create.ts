/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { EventsSchema } from '@/Events/infrastructure/EventsMySqlRepository/schema'
import UserSchema from '@/Users/infrastructure/repositories/UserMySqlRepository/schema'
import { Controller } from '@/shared/controller'
import { type Status } from '@/shared/status'
import type { Response } from 'express'
import { BookingSchema } from '../entity'
import { isDefined, isInt } from '@/shared/validators'

interface BookingData {
  userId: number
  eventId: number
}

export default class CreateController extends Controller {
  private async __get_user__(id: number): Promise<UserSchema | null> {
    try {
      return await UserSchema.findOneBy({ id })
    } catch (error) {
      console.log(error)
      return null
    }
  }

  private async __get_event__(id: number): Promise<EventsSchema | null> {
    try {
      return await EventsSchema.findOneBy({ id })
    } catch (error) {
      console.log(error)
      return null
    }
  }

  private __get_booking__(
    user: UserSchema,
    event: EventsSchema
  ): BookingSchema {
    const now = new Date()

    const booking = new BookingSchema()
    booking.userId = user.id
    booking.eventId = event.id
    booking.dateTime = now.toISOString()
    return booking
  }

  private ___error__(code: number, message: string): Status<undefined> {
    return { code, isError: true, message }
  }

  private __send_status__(res: Response, status: Status<any>): void {
    res.status(status.code).send(status)
  }

  private __check__(u: any, e: any): Status<undefined> | null {
    if (e == null) return this.___error__(400, 'event not found')
    if (u == null) return this.___error__(400, 'user not found')
    return null
  }

  private async __bookings__(eventId: number): Promise<number | null> {
    try {
      return await BookingSchema.count({ where: { eventId } })
    } catch (error) {
      console.log(error)
      return null
    }
  }

  private async __save__(
    booking: BookingSchema
  ): Promise<Status<undefined> | null> {
    try {
      await booking.save()
      return null
    } catch (error) {
      console.log(error)
      return this.___error__(500, 'fail save booking')
    }
  }

  private __get_params_with_body__(body: any): BookingData | null {
    const { userId: _userId, eventId: _eventId } = body

    const userId = Number(_userId)
    const eventId = Number(_eventId)

    if (!isInt(userId) || !isInt(eventId)) return null
    return { userId, eventId }
  }

  private __get_params_with_params__(req: any): BookingData | null {
    const { userId } = req.user
    const { eventId: _eventId } = req.params

    const eventId = Number(_eventId)
    if (isNaN(eventId)) return null

    return { userId, eventId }
  }

  private __get_params__(req: any): BookingData | null {
    const body = req.body
    const { eventId } = req.params

    if (isDefined(eventId)) return this.__get_params_with_params__(req)
    else return this.__get_params_with_body__(body)
  }

  async handler(req: any, res: Response): Promise<void> {
    const params = this.__get_params__(req)
    if (params == null) {
      const error = this.___error__(500, 'params not valid')
      this.__send_status__(res, error)
      return
    }

    const { userId, eventId } = params

    const bookings = await this.__bookings__(eventId)
    if (bookings === null) {
      const error = this.___error__(500, 'fail db connect')
      this.__send_status__(res, error)
      return
    }

    const user = await this.__get_user__(userId)
    const event = await this.__get_event__(Number(eventId))

    const resultCheck = this.__check__(user, event)
    if (resultCheck != null) {
      this.__send_status__(res, resultCheck)
      return
    }

    if (bookings === event?.limit) {
      const error = this.___error__(400, 'limit event')
      this.__send_status__(res, error)
      return
    }

    const booking = this.__get_booking__(user!, event!)
    const resultSave = await this.__save__(booking)
    if (resultSave != null) {
      this.__send_status__(res, resultSave)
      return
    }

    res.status(200).send({ message: 'save booking' })
  }
}
