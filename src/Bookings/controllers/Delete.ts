import { Controller } from '@/shared/controller'
import type { Status } from '@/shared/status'
import type { Request, Response } from 'express'
import { BookingSchema } from '../entity'

export default class DeleteController extends Controller {
  private ___error__(code: number, message: string): Status<undefined> {
    return { code, isError: true, message }
  }

  private __send_status__(res: Response, status: Status<any>): void {
    res.status(status.code).send(status)
  }

  private async __delete__(id: number): Promise<Status<any>> {
    try {
      const { affected } = await BookingSchema.delete(id)

      if (affected === 0 || affected == null) {
        return { isError: true, code: 400, message: 'booking not found' }
      }

      return { isError: false, code: 200, message: 'delete booking' }
    } catch (error) {
      console.log(error)
      return { isError: true, code: 500, message: 'db not found' }
    }
  }

  async handler(req: Request, res: Response): Promise<void> {
    const { id: _id } = req.params
    const id = Number(_id)

    if (isNaN(id)) {
      const error = this.___error__(400, 'id not valid')
      this.__send_status__(res, error)
      return
    }

    const result = await this.__delete__(id)
    this.__send_status__(res, result)
  }
}
