import { Controller } from '@/shared/controller'
import {
  type PaginatedResponse,
  type PaginatedDto,
  PaginatedMapper,
} from '@/shared/paginated'
import { type Status } from '@/shared/status'
import type { Request, Response } from 'express'
import { BookingSchema } from '../entity'
import { isDefined } from '@/shared/validators'
import { EventsSchema } from '@/Events/infrastructure/EventsMySqlRepository/schema'
import UserSchema from '@/Users/infrastructure/repositories/UserMySqlRepository/schema'

const LIMIT = 20
export default class SearchController extends Controller {
  private __isEmpty__(o: any): boolean {
    return o == null || Object.keys(o).length === 0
  }

  private ___error__(code: number, message: string): Status<undefined> {
    return { code, isError: true, message }
  }

  private __send_status__(res: Response, status: Status<any>): void {
    res.status(status.code).send(status)
  }

  private __paginated__(
    bookings: BookingSchema[],
    count: number,
    page: number
  ): PaginatedDto<BookingSchema> {
    const pages = Math.ceil(count / LIMIT)

    return { count, list: bookings, page, pages }
  }

  private async __get_all__(
    page: number
  ): Promise<PaginatedDto<BookingSchema> | null> {
    const skip = (page - 1) * LIMIT
    const take = LIMIT
    try {
      const [bookings, count] = await BookingSchema.findAndCount({
        skip,
        take,
      })

      return this.__paginated__(bookings, count, page)
    } catch (error) {
      return null
    }
  }

  private async __find__(
    params: Partial<BookingSchema>,
    page: number
  ): Promise<PaginatedDto<BookingSchema> | null> {
    const skip = (page - 1) * LIMIT
    const take = LIMIT
    const where = {
      id: params.id,
      userId: params.userId,
      eventId: params.eventId,
      dateTime: params.dateTime,
    }

    if (where.id == null) delete where.id
    if (where.userId == null) delete where.userId
    if (where.eventId == null) delete where.eventId
    if (where.dateTime == null) delete where.dateTime

    try {
      const [bookings, count] = await BookingSchema.findAndCount({
        where,
        skip,
        take,
      })

      return this.__paginated__(bookings, count, page)
    } catch (error) {
      return null
    }
  }

  private __get_url__(req: Request): string {
    const protocol = req.protocol
    const host = req.get('host') as string
    const root = req.baseUrl

    return protocol + '://' + host + root + '/users'
  }

  private __response__(
    bookings: PaginatedDto<BookingSchema>,
    url: string
  ): PaginatedResponse<BookingSchema> {
    const mapper = new PaginatedMapper(url)
    return mapper.toPaginatedResponse(bookings)
  }

  private async __get_bookings__(
    params: any,
    page: number
  ): Promise<PaginatedDto<BookingSchema> | null> {
    let bookings: PaginatedDto<BookingSchema> | null = null
    if (this.__isEmpty__(params)) {
      bookings = await this.__get_all__(page)
    } else {
      bookings = await this.__find__(params, page)
    }
    return bookings
  }

  private __get_page__(page: any): number {
    let _page = Number(page)
    if (isNaN(_page)) {
      _page = 1
    }
    return _page
  }

  private __get_params__(req: Request): {
    page: number
    params: Partial<BookingSchema>
    isId: boolean
  } {
    const body = req.body
    const { page: _page } = req.query
    const { userId } = req.params

    const page = this.__get_page__(_page)

    const params: Partial<BookingSchema> = body
    let isId = false
    if (isDefined(userId)) {
      isId = true
      params.userId = Number(userId)
    }

    return { page, isId, params }
  }

  private async __parse__(
    resp: PaginatedResponse<BookingSchema>
  ): Promise<any> {
    const bookings = resp.result
    const result = []

    for (const booking of bookings) {
      const event = await EventsSchema.findOneBy({ id: booking.eventId })
      const user = await UserSchema.findOneBy({ id: booking.userId })
      const parseBooking = {
        id: booking.id,
        user: user?.username,
        event: event?.name,
        location: event?.place,
        paymentDate: booking.dateTime,
        coords: {
          latitude: event?.gps.latitude,
          longitude: event?.gps.longitude,
        },
      }
      result.push(parseBooking)
    }

    return { ...resp, result }
  }

  async handler(req: Request, res: Response): Promise<void> {
    const { params, page, isId } = this.__get_params__(req)

    const bookings = await this.__get_bookings__(params, page)

    if (bookings == null) {
      const error = this.___error__(500, 'fail get bookings')
      this.__send_status__(res, error)
      return
    }

    const url = this.__get_url__(req)
    let response: any = this.__response__(bookings, url)
    if (isId) {
      response = await this.__parse__(response)
    }

    res.send(response)
  }
}
