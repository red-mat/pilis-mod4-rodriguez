import { Controller } from '@/shared/controller'
import type { Status } from '@/shared/status'
import type { Request, Response } from 'express'
import { BookingSchema } from '../entity'
import { EventsSchema } from '@/Events/infrastructure/EventsMySqlRepository/schema'
import UserSchema from '@/Users/infrastructure/repositories/UserMySqlRepository/schema'
import { isDefined, isInt } from '@/shared/validators'
import { isDateTime } from '@/Events/core/utils/validate'

interface Booking {
  id: number
  userId: number
  eventId: number
  dateTime: string
}

export default class UpdateController extends Controller {
  private __error__(code: number, message: string): Status<undefined> {
    return { code, isError: true, message }
  }

  private __send_status__(res: Response, status: Status<any>): void {
    res.status(status.code).send(status)
  }

  private async __check_event__(id: number): Promise<boolean> {
    try {
      const count = await EventsSchema.count({ where: { id } })
      return count > 0
    } catch (error) {
      console.log(error)
      return false
    }
  }

  private async __check_user__(id: number): Promise<boolean> {
    try {
      const count = await UserSchema.count({ where: { id } })
      return count > 0
    } catch (error) {
      console.log(error)
      return false
    }
  }

  private async __get_query__(
    params: Partial<Booking>
  ): Promise<Partial<Booking> | null> {
    const where: Partial<Booking> = {}

    if (isDefined(params.id)) {
      const id = Number(params.id)
      if (!isInt(id)) return null
      where.id = id
    }

    if (isDefined(params.dateTime)) {
      if (isDateTime(params.dateTime)) return null
      where.dateTime = params.dateTime
    }

    if (isDefined(params.eventId)) {
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      const isEvent = await this.__check_event__(params.eventId!)
      if (!isEvent) return null

      where.eventId = params.eventId
    }

    if (isDefined(params.userId)) {
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      const isEvent = await this.__check_user__(params.userId!)
      if (!isEvent) return null

      where.userId = params.userId
    }

    return where
  }

  private async __update__(
    where: Partial<Booking>
  ): Promise<Status<undefined>> {
    try {
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      await BookingSchema.update(where.id!, where)
      return { code: 200, isError: false, message: 'update booking' }
    } catch (error) {
      console.log(error)
      return this.__error__(500, 'fail update booking')
    }
  }

  async handler(req: Request, res: Response): Promise<void> {
    const { id } = req.body

    if (!isDefined(id)) {
      const error = this.__error__(400, 'insert booking id')
      this.__send_status__(res, error)
      return
    }

    const query = await this.__get_query__(req.body)
    if (!isDefined(query)) {
      const error = this.__error__(400, 'params not valid')
      this.__send_status__(res, error)
      return
    }

    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    const result = await this.__update__(query!)
    this.__send_status__(res, result)
  }
}
