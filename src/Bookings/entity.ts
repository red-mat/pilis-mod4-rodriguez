import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from 'typeorm'

@Entity()
export class BookingSchema extends BaseEntity {
  @PrimaryGeneratedColumn()
  id!: number

  @Column({ type: 'int' })
  eventId!: number

  @Column({ type: 'int' })
  userId!: number

  @Column({ type: 'datetime' })
  dateTime!: string
}
