/* eslint-disable @typescript-eslint/no-misused-promises */

import auth from '@/Users/infrastructure/middleware/auth'
import { Router } from 'express'
import { Create, Delete, Search, Update } from './controllers'

const PATH = '/bookings'
const route = Router()

route.use(PATH, auth)

const searchCtrl = new Search(PATH)
route.get(searchCtrl.path, searchCtrl.handler)
route.get(searchCtrl.withParam('userId'), searchCtrl.handler)

const createCtrl = new Create(PATH)
route.post(createCtrl.path, createCtrl.handler)
route.post(createCtrl.withParam('eventId'), createCtrl.handler)

const updateCtrl = new Update(PATH)
route.put(updateCtrl.path, updateCtrl.handler)

const deleteCtrl = new Delete(PATH)
route.delete(deleteCtrl.withParam('id'), deleteCtrl.handler)

export { route as bookings }
