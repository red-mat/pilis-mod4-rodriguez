import { Router } from 'express'
import swaggerJSDoc from 'swagger-jsdoc'
import swaggerUi from 'swagger-ui-express'

import options from './options'

const PATH = '/docs'
const ApiDocsRouter = Router()

const specs = swaggerJSDoc(options)
ApiDocsRouter.use(PATH, swaggerUi.serve, swaggerUi.setup(specs))

export default ApiDocsRouter
