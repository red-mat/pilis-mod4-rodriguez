import path from 'path'
import environment from '../shared/environment'

export default {
  definition: {
    openapi: '3.0.0',
    info: {
      title: 'API',
      version: '1.0.0',
      description: 'api de reserva de tickets',
    },
    servers: [
      {
        url: environment.DOMAIN ?? 'http://localhost:5000',
      },
    ],
  },
  apis: [path.join(__dirname, '/docs/**/*.yml')],
}
