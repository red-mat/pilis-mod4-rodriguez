import express from 'express'
import morgan from 'morgan'
import ApiDocsRouter from './ApiDocs/route'
import environment from './shared/environment'
import { auth, users } from './Users/infrastructure/routes'
import { events } from './Events/infrastructure/routes'
import { bookings } from './Bookings/route'

const PATH = environment.ROOT_PATH
const app = express()

// *** middleware ***
app.use(morgan('dev'))
app.use(express.json())

// *** routes ***
// * Docs route
app.use(PATH, ApiDocsRouter)
app.use(PATH, users)
app.use(PATH, auth)
app.use(PATH, events)
app.use(PATH, bookings)

// * Default route
app.use(function (req, res) {
  res.sendStatus(404)
})

export default app
