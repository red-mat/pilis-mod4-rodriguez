import 'module-alias/register'

import app from './app'
import environment from './shared/environment'
import DataSource from './shared/typeorm/DataSource'

const PORT = environment.PORT ?? 5000

function onListening(): void {
  console.log(`App listening on port ${PORT}`)
}

async function main(): Promise<void> {
  try {
    await DataSource.initialize()
    app.listen(PORT, onListening)
  } catch (error) {
    console.error(error)
  }
}

void main()
