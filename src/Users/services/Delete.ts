import { type Status } from '@/shared/status'
import { type UserRepository } from '../core/models'

export default class Delete {
  constructor(private readonly repository: UserRepository) {}

  async run(id: number): Promise<Status<undefined>> {
    try {
      const result = await this.repository.del(id)
      return result
    } catch (error) {
      return { code: 500, isError: true, message: (error as Error).message }
    }
  }
}
