/* eslint-disable @typescript-eslint/strict-boolean-expressions */
import { type Status } from '@/shared/status'
import {
  UserModelDto,
  type IUserCredentialsDto,
  type IUserModelDto,
  type UserCredentialsDto,
} from '../core/dto'
import { Auth, type UserRepository } from '../core/models'
import UserCredentials from '../core/models/UserCredentials'
import { isInstance, isString } from '@/shared/validators'

type ErrorResult = Status<undefined>
type Result = Promise<Status<string> | ErrorResult>

export default class SignIn {
  constructor(private readonly _repository: UserRepository) {}

  private async __get_user__(
    credentials: UserCredentials
  ): Promise<IUserModelDto | ErrorResult> {
    const { username } = credentials.toDto()
    const result = await this._repository.search({ username })

    if (result.isError) {
      console.log(result.message)
      return { code: 500, message: 'fail get user', isError: true }
    }
    if (!result.payload?.count)
      return {
        code: 404,
        message: 'Incorrect username or password',
        isError: true,
      }

    const dto = result.payload.list[0]
    return new UserModelDto(dto)
  }

  private __get_credentials__(
    credentials: IUserCredentialsDto
  ): UserCredentials | ErrorResult {
    try {
      return new UserCredentials(credentials)
    } catch (error) {
      console.log((error as Error).message)
      return { code: 404, isError: true, message: 'credentials not valid' }
    }
  }

  private __get_token__(
    user: UserModelDto,
    credentials: UserCredentialsDto
  ): string | ErrorResult {
    try {
      const auth = new Auth(user)
      return auth.signIn(credentials)
    } catch (error) {
      console.log((error as Error).message)
      return {
        code: 404,
        isError: true,
        message: 'Incorrect username or password',
      }
    }
  }

  async run(credentialsDto: IUserCredentialsDto): Result {
    const credentialsResult = this.__get_credentials__(credentialsDto)
    if (!isInstance(credentialsResult, UserCredentials))
      return credentialsResult as ErrorResult

    const userResult = await this.__get_user__(
      credentialsResult as UserCredentials
    )
    if (!isInstance(userResult, UserModelDto)) return userResult as ErrorResult

    const tokeResult = this.__get_token__(
      userResult as UserModelDto,
      (credentialsResult as UserCredentials).toDto()
    )
    if (!isString(tokeResult as any)) return tokeResult as ErrorResult

    return {
      code: 200,
      isError: false,
      payload: tokeResult as string,
      message: 'Success',
    }
  }
}
