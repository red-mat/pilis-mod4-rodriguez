/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { type Status } from '@/shared/status'
import { UserModelDto, type IUserModelDto } from '../core/dto'
import { User, UserModel, type UserRepository } from '../core/models'
import { isDefined, isInstance } from '@/shared/validators'

type Result = Promise<Status<undefined>>
type ErrorResult = Status<undefined>

export default class Update {
  constructor(private readonly repository: UserRepository) {}

  async __get__user__(id?: number): Promise<UserModel | ErrorResult> {
    if (!isDefined(id))
      return { code: 404, isError: true, message: 'user id undefined' }

    const { payload, isError, message } = await this.repository.search({ id })
    if (!isDefined(payload) || isError) {
      console.log(message)
      return { code: 404, isError: true, message: 'user id not found' }
    }

    const { count, list } = payload!
    if (count === 0)
      return { code: 404, isError: true, message: 'user id not found' }

    const userModelDto = list[0]
    return new UserModel(userModelDto)
  }

  private __update_data__(
    userModel: UserModel,
    data: Partial<IUserModelDto>
  ): UserModelDto | ErrorResult {
    const { password, username, id } = data
    const userDto = userModel.toUserDto()
    const user = new User(userDto)

    try {
      if (isDefined(username)) user.setUsername(username!)
    } catch (error) {
      console.log((error as Error).message)
      return { code: 404, isError: true, message: 'user name not valid' }
    }

    try {
      if (isDefined(password)) user.setPassword(password!)
    } catch (error) {
      console.log((error as Error).message)
      return { code: 404, isError: true, message: 'user name not valid' }
    }

    return user.toUserRepositoryDto(Number(id))
  }

  async run(userDto: Partial<IUserModelDto>): Result {
    try {
      const resultGetUser = await this.__get__user__(userDto.id)

      if (!isInstance(resultGetUser, UserModel))
        return resultGetUser as ErrorResult

      const userModel = resultGetUser as UserModel
      const resultGetModelDto = this.__update_data__(userModel, userDto)

      if (!isInstance(resultGetModelDto, UserModelDto))
        return resultGetModelDto as ErrorResult

      return await this.repository.update(resultGetModelDto as UserModelDto)
    } catch (error) {
      return { code: 500, isError: true, message: 'fail update user' }
    }
  }
}
