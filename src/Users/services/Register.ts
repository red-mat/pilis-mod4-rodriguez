import type { Status } from '@/shared/status'
import { type IUserDto } from '../core/dto'
import { Auth, User, type UserRepository } from '../core/models'
import { isDefined, isInstance } from '@/shared/validators'

type Result = Promise<Status<undefined>>
type ErrorResult = Status<undefined>

export default class Register {
  constructor(private readonly _repository: UserRepository) {}

  private async __generate_user__(
    userDto: IUserDto
  ): Promise<User | ErrorResult> {
    const { username } = userDto
    const result = await this._repository.search({ username })

    if (isDefined(result.payload) && result.payload?.count !== 0)
      return { code: 400, isError: true, message: 'username not valid' }

    try {
      return Auth.SignUp(userDto)
    } catch (error) {
      console.log((error as Error).message)
      return { code: 404, isError: true, message: 'user data not valid' }
    }
  }

  async run(userDto: IUserDto): Result {
    try {
      const userResult = await this.__generate_user__(userDto)
      if (!isInstance(userResult, User)) return userResult as ErrorResult

      return await this._repository.add((userResult as User).toDto())
    } catch (e) {
      console.log((e as Error).message)
      return { code: 500, isError: true, message: 'fail register user' }
    }
  }
}
