import { type PaginatedDto } from '@/shared/paginated'
import { type Status } from '@/shared/status'
import { isDefined } from '@/shared/validators'
import {
  type UserDataDto,
  type IUserDataDto,
  type IUserModelDto,
} from '../core/dto'
import { UserModel, type UserRepository } from '../core/models'

type ResultUser = Status<PaginatedDto<UserDataDto>>
type Result = Promise<ResultUser>

export default class Search {
  constructor(private readonly repository: UserRepository) {}

  private __resolve__(result: Status<PaginatedDto<IUserModelDto>>): ResultUser {
    const { payload } = result
    if (!isDefined(payload)) throw new Error('payload undefined')

    const users = payload?.list.map(u => new UserModel(u).toUserDataDto())

    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    return { ...result, payload: { ...payload!, list: users! } }
  }

  async run(user: Partial<IUserDataDto>, page = 1): Result {
    const result = await this.repository.search(user, page)
    return this.__resolve__(result)
  }
}
