export interface IUserDto {
  username: string
  password: string
}

export class UserDto implements IUserDto {
  readonly username: string
  readonly password: string

  constructor({ username, password }: IUserDto) {
    this.username = username
    this.password = password
  }
}
