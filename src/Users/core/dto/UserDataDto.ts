export interface IUserDataDto {
  id: number
  username: string
}

export class UserDataDto implements IUserDataDto {
  readonly id: number
  readonly username: string

  constructor({ id, username }: IUserDataDto) {
    this.id = id
    this.username = username
  }
}
