export interface IUserCredentialsDto {
  username: string
  password: string
}

export class UserCredentialsDto implements IUserCredentialsDto {
  readonly username: string
  readonly password: string

  constructor({ username, password }: IUserCredentialsDto) {
    this.username = username
    this.password = password
  }
}
