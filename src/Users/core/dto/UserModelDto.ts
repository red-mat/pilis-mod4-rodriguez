import type { IUserDto } from './UserDto'

export interface IUserModelDto extends IUserDto {
  id: number
}

export class UserModelDto implements IUserModelDto {
  readonly id: number
  readonly username: string
  readonly password: string

  constructor({ id, username, password }: IUserModelDto) {
    this.id = id
    this.username = username
    this.password = password
  }
}
