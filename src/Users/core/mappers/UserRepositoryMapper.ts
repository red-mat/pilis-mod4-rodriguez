import {
  UserCredentialsDto,
  UserDataDto,
  UserDto,
  type UserModelDto,
} from '../dto'

export default class UserRepositoryMapper {
  private readonly _dto: UserModelDto

  constructor(dto: UserModelDto) {
    this._dto = dto
  }

  toUserDto(): UserDto {
    const { username, password } = structuredClone(this._dto)
    return new UserDto({ username, password })
  }

  toUserDataDto(): UserDataDto {
    const { id, username } = structuredClone(this._dto)
    return new UserDataDto({ id, username })
  }

  toUserCredentialDto(): UserCredentialsDto {
    const { username, password } = structuredClone(this._dto)
    return new UserCredentialsDto({ username, password })
  }
}
