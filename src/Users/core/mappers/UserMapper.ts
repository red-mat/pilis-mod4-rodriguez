import {
  UserModelDto,
  type UserDto,
  UserDataDto,
  UserCredentialsDto,
} from '../dto'
import { isId } from '../utils/validate'

export default class UserMapper {
  private readonly _dto: UserDto

  constructor(dto: UserDto) {
    this._dto = dto
  }

  toUserRepositoryDto(id: number): UserModelDto {
    if (!isId(id)) throw new Error('not valid id')
    const cloneDto = structuredClone(this._dto)

    return new UserModelDto({ id, ...cloneDto })
  }

  toUserDataDto(id: number): UserDataDto {
    if (!isId(id)) throw new Error('not valid id')
    const cloneDto = structuredClone(this._dto)

    return new UserDataDto({ id, ...cloneDto })
  }

  toUserCredentialDto(): UserCredentialsDto {
    const { username, password } = structuredClone(this._dto)
    return new UserCredentialsDto({ username, password })
  }
}
