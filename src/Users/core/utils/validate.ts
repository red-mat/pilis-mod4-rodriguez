import { isNumber, isString } from '@/shared/validators'

const PATTERN_USERNAME = /^[a-z0-9]+$/
const PATTERN_PASSWORD = /^[a-zA-Z0-9!@#$%^&*()-_=+{}[\]:;"'<>,.?\\/]{8,}$/

export const isId = (id: number): boolean => isNumber(id)
export const isUsername = (username: string): boolean =>
  isString(username) && PATTERN_USERNAME.test(username)
export const isPassword = (password: string): boolean =>
  isString(password) && PATTERN_PASSWORD.test(password)
