import { Repository } from '@/shared/repository'
import type { UserDto, UserModelDto } from '../dto'

export default abstract class UserRepository extends Repository<
  UserModelDto,
  UserDto,
  number
> {}
