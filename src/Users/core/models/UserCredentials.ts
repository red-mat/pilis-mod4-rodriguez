import bcrypt from 'bcrypt'
import { type IUserCredentialsDto, UserCredentialsDto } from '../dto'
import { isPassword, isUsername } from '../utils/validate'

export default class UserCredentials {
  private readonly _data: UserCredentialsDto

  constructor(credentialsDto: IUserCredentialsDto) {
    this._data = this.__validate__(credentialsDto)
  }

  private __validate__(
    credentialsDto: IUserCredentialsDto
  ): UserCredentialsDto {
    const { username, password } = credentialsDto
    if (!isUsername(username) && !isPassword(password))
      throw new Error('credentials not valid')

    return new UserCredentialsDto(credentialsDto)
  }

  validate(credentials: UserCredentialsDto): boolean {
    const { username, password } = this._data

    return (
      credentials.username === username &&
      bcrypt.compareSync(password, credentials.password)
    )
  }

  toDto(): UserCredentialsDto {
    const data = structuredClone(this._data)
    return new UserCredentialsDto(data)
  }
}
