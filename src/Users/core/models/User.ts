import bcrypt from 'bcrypt'
import { UserMapper } from '../mappers'
import { isPassword, isUsername } from '../utils/validate'
import {
  UserDto,
  type IUserDto,
  type UserModelDto,
  type UserDataDto,
  type UserCredentialsDto,
} from '../dto'

export default class User {
  private _data: UserDto
  private _mapper: UserMapper

  constructor(userDto: IUserDto) {
    this._data = this.__validate__(userDto)
    this._mapper = new UserMapper(this._data)
  }

  private __validate__(dto: IUserDto): UserDto {
    const { username, password } = dto

    if (!isUsername(username)) throw new Error('not valid user')
    if (!isPassword(password)) throw new Error('not valid user')

    return new UserDto(dto)
  }

  private __hashPassword__(password: string): string {
    const saltRounds = 10
    return bcrypt.hashSync(password, saltRounds)
  }

  private __update__(data: IUserDto): void {
    this._data = new UserDto(data)
    this._mapper = new UserMapper(this._data)
  }

  setUsername(username: string): User {
    const { password } = structuredClone(this._data)
    if (!isUsername(username)) throw new Error('username not valid')

    this.__update__({ username, password })

    return this
  }

  getPassword(): string {
    const { password } = structuredClone(this._data)
    return password
  }

  setPassword(newPassword: string): User {
    const { username } = structuredClone(this._data)
    if (!isPassword(newPassword)) throw new Error('password not valid')

    const password = this.__hashPassword__(newPassword)
    this.__update__({ username, password })

    return this
  }

  toDto(): UserDto {
    return structuredClone(this._data)
  }

  toUserRepositoryDto(id: number): UserModelDto {
    return this._mapper.toUserRepositoryDto(id)
  }

  toUserDataDto(id: number): UserDataDto {
    return this._mapper.toUserDataDto(id)
  }

  toUserCredentialsDto(): UserCredentialsDto {
    return this._mapper.toUserCredentialDto()
  }
}
