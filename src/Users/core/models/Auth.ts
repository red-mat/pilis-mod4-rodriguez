import jwt from 'jsonwebtoken'

import {
  type IUserCredentialsDto,
  type IUserDto,
  type IUserModelDto,
} from '../dto'
import User from './User'
import UserCredentials from './UserCredentials'
import UserModel from './UserModel'

type Token = string

export default class Auth {
  private readonly _user: UserModel

  static SignUp(UserDto: IUserDto): User {
    const user = new User(UserDto)

    const { password } = user.toDto()
    user.setPassword(password)

    return user
  }

  constructor(userDto: IUserModelDto) {
    this._user = new UserModel(userDto)
  }

  private __validate__(credentialsDto: IUserCredentialsDto): boolean {
    const credentials = new UserCredentials(credentialsDto)
    const dto = this._user.toUserCredentialsDto()

    return credentials.validate(dto)
  }

  signIn(credentials: IUserCredentialsDto): Token {
    if (!this.__validate__(credentials))
      throw new Error('credentials not valid')

    const { id, username } = this._user.toUserDataDto()

    const payload = { id, username }
    const jwtHash = 'secret_token'
    const expiresIn = 60 * 60 * 24

    return jwt.sign(payload, jwtHash, { expiresIn })
  }
}
