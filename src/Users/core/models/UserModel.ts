import { UserRepositoryMapper } from '../mappers'
import { isId, isPassword, isUsername } from '../utils/validate'
import {
  UserModelDto,
  type IUserModelDto,
  type UserDto,
  type UserDataDto,
  type UserCredentialsDto,
} from '../dto'

export default class UserModel {
  private readonly _data: UserModelDto
  private readonly _mapper: UserRepositoryMapper

  constructor(userRepositoryDto: IUserModelDto) {
    this._data = this.__validate__(userRepositoryDto)
    this._mapper = new UserRepositoryMapper(this._data)
  }

  private __validate__(dto: IUserModelDto): UserModelDto {
    const { id, username, password } = dto

    if (!isId(id)) throw new Error('not valid user')
    if (!isUsername(username)) throw new Error('not valid user')
    if (!isPassword(password)) throw new Error('not valid user')

    return new UserModelDto(dto)
  }

  toDto(): UserModelDto {
    return structuredClone(this._data)
  }

  toUserDto(): UserDto {
    return this._mapper.toUserDto()
  }

  toUserDataDto(): UserDataDto {
    return this._mapper.toUserDataDto()
  }

  toUserCredentialsDto(): UserCredentialsDto {
    return this._mapper.toUserCredentialDto()
  }
}
