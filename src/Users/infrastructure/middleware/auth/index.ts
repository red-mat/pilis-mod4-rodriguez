/* eslint-disable @typescript-eslint/explicit-function-return-type */
import passport from 'passport'
import Strategy from './Strategy'

passport.use(Strategy)
export default passport.authenticate('jwt', { session: false })
