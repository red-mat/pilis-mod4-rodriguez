/* eslint-disable @typescript-eslint/no-non-null-assertion */
/* eslint-disable @typescript-eslint/no-confusing-void-expression */
/* eslint-disable @typescript-eslint/no-misused-promises */
import { type IUserDataDto } from '@/Users/core/dto'
import { ExtractJwt, Strategy, type VerifiedCallback } from 'passport-jwt'
import { UserMySqlRepository } from '../../repositories'
import { Search } from '@/Users/services'
import { isDefined } from '@/shared/validators'

const options = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: 'secret_token',
}

async function verifyCallback(
  payload: IUserDataDto,
  done: VerifiedCallback
): Promise<void> {
  const { id } = payload
  const repository = new UserMySqlRepository()
  const search = new Search(repository)
  try {
    const { isError, payload } = await search.run({ id: Number(id) })
    if (isError || !isDefined(payload)) return done(null, false)

    const { count, list } = payload!
    if (count === 0) return done(null, false)

    return done(null, list[0])
  } catch (error) {
    done(error, false)
  }
}

export default new Strategy(options, verifyCallback)
