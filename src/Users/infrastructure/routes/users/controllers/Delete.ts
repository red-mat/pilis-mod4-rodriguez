import { UserMySqlRepository } from '@/Users/infrastructure/repositories'
import { Delete } from '@/Users/services'
import { Controller } from '@/shared/controller'
import { type Request, type Response } from 'express'

export default class Create extends Controller {
  handler(req: Request, res: Response): void {
    const { id } = req.params

    try {
      const repository = new UserMySqlRepository()
      const deleteService = new Delete(repository)
      deleteService
        .run(Number(id))
        .then(r => res.send({ code: r.code, message: r.message ?? null }))
        .catch(() => {
          res.sendStatus(400)
        })
    } catch (error) {
      console.log((error as Error).message)
      res.sendStatus(500)
    }
  }
}
