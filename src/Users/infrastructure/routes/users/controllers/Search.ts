/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { type Request, type Response } from 'express'
import { Controller } from '@/shared/controller'
import { UserMySqlRepository } from '@/Users/infrastructure/repositories'
import Search from '@/Users/services/Search'
import { isDefined, isInt } from '@/shared/validators'
import {
  type PaginatedDto,
  PaginatedMapper,
  type PaginatedResponse,
} from '@/shared/paginated'
import { type IUserDataDto, type UserDataDto } from '@/Users/core/dto'

export default class SearchController extends Controller {
  private __get_page__(page: any): number {
    const pageNumber = Number(page)
    if (!isInt(pageNumber)) return 1
    return pageNumber
  }

  private __paginated__(
    paginated: PaginatedDto<UserDataDto>,
    url: string
  ): PaginatedResponse<IUserDataDto> {
    const mapper = new PaginatedMapper(url)
    return mapper.toPaginatedResponse(paginated)
  }

  private __get_url__(req: Request): string {
    const protocol = req.protocol
    const host = req.get('host') as string
    const root = req.baseUrl

    return protocol + '://' + host + root + '/users'
  }

  handler(req: Request, res: Response): void {
    const page = this.__get_page__(req.query.page)
    const repository = new UserMySqlRepository()
    const search = new Search(repository)

    search
      .run(req.body, page)
      .then(result => {
        if (!isDefined(result.payload)) res.status(result.code).send(result)
        else {
          const url = this.__get_url__(req)
          const payload = this.__paginated__(result.payload!, url)
          res.status(result.code).send({ ...result, payload })
        }
      })
      .catch(() => res.sendStatus(500))
  }
}
