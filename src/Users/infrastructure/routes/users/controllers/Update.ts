import { type Request, type Response } from 'express'
import { Controller } from '@/shared/controller'
import { UserMySqlRepository } from '@/Users/infrastructure/repositories'
import { Update } from '@/Users/services'

export default class SearchController extends Controller {
  handler(req: Request, res: Response): void {
    const repository = new UserMySqlRepository()
    const update = new Update(repository)
    update
      .run(req.body)
      .then(result => res.status(result.code).send(result))
      .catch(() => res.sendStatus(500))
  }
}
