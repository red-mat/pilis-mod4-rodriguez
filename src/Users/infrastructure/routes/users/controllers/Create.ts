import { UserMySqlRepository } from '@/Users/infrastructure/repositories'
import { Register } from '@/Users/services'
import { Controller } from '@/shared/controller'
import { type Request, type Response } from 'express'

export default class Create extends Controller {
  handler(req: Request, res: Response): void {
    const repository = new UserMySqlRepository()
    const register = new Register(repository)
    register
      .run(req.body)
      .then(result => res.status(result.code).send(result))
      .catch(() => {
        res.sendStatus(500)
      })
  }
}
