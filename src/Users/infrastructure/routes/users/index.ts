/* eslint-disable @typescript-eslint/no-misused-promises */
import { Router } from 'express'
import {
  Create,
  Delete,
  Search as SearchController,
  Update,
} from './controllers'
import auth from '../../middleware/auth'

const PATH = '/users'

const route = Router()

route.use(PATH, auth)

const search = new SearchController(PATH)
route.get(search.path, search.handler)

const create = new Create(PATH)
route.post(create.path, create.handler)

const del = new Delete(PATH)
route.delete(del.withParam('id'), del.handler)

const update = new Update(PATH)
route.put(update.path, update.handler)

export default route
