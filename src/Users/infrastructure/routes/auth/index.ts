 
import { Router } from 'express'
import { SignInController, SignUpController } from './controllers'

const PATH = '/auth'

const route = Router()

const signIn = new SignInController(PATH)
route.get(signIn.path, signIn.handler)

const signUp = new SignUpController(PATH)
route.post(signUp.path, signUp.handler)

export default route
