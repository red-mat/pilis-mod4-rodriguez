import { type Request, type Response } from 'express'
import { Controller } from '@/shared/controller'
import { SignIn } from '@/Users/services'
import { UserMySqlRepository } from '@/Users/infrastructure/repositories'

export default class SignInController extends Controller {
  handler(req: Request, res: Response): void {
    const repository = new UserMySqlRepository()
    const signIn = new SignIn(repository)
    signIn
      .run(req.body)
      .then(result =>
        res.status(result.code).send({
          code: result.code,
          isError: result.isError,
          message: result.message,
          token: result.payload,
        })
      )
      .catch(e => {
        console.log(e)
        res.sendStatus(500)
      })
  }
}
