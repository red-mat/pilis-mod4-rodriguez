import {
  type IUserModelDto,
  type UserDto,
  type UserModelDto,
} from '@/Users/core/dto'
import { UserRepository } from '@/Users/core/models'
import { type PaginatedDto } from '@/shared/paginated'
import { type Response, type ResponseSearch } from '@/shared/repository'

const USERS: IUserModelDto[] = [
  {
    id: 1,
    username: 'user1',
    password: '$2b$10$bMxskjg5VP5qC6EEn3EwTu8t3K1/kLzGoXCab0UAOiwj4T7q1Y7.m',
  },
]

export default class UserMockRepository extends UserRepository {
  private __search__(e: Partial<UserModelDto>): IUserModelDto[] {
    return USERS.filter(
      u =>
        u.id === e.id || u.password === e.password || u.username === e.username
    )
  }

  private __code__(users: IUserModelDto[]): number {
    if (users.length === 0) return 404
    else return 200
  }

  private __data__(
    users: IUserModelDto[],
    page: number
  ): PaginatedDto<IUserModelDto> {
    const count = users.length
    const list = users
    const pages = Math.ceil(count / 20)
    return { count, list, page, pages }
  }

  async add(e: UserDto): Response {
    throw new Error('Method not implemented.')
  }

  async del(id: number): Response {
    throw new Error('Method not implemented.')
  }

  async update(e: UserModelDto): Response {
    throw new Error('Method not implemented.')
  }

  async search(
    e: Partial<UserModelDto>,
    page: number = 1
  ): ResponseSearch<UserModelDto> {
    const users = this.__search__(e)
    const payload = this.__data__(users, page)
    const code = this.__code__(users)

    return { code, payload, isError: false }
  }
}
