import { UserModelDto, type UserDto } from '@/Users/core/dto'
import { UserRepository } from '@/Users/core/models'
import { type Response, type ResponseSearch } from '@/shared/repository'
import UserSchema from './schema'
import { isDefined } from '@/shared/validators'
import { type PaginatedDto } from '@/shared/paginated'
import { BookingSchema } from '@/Bookings/entity'

const LIMIT = 20

export default class UserMySqlRepository extends UserRepository {
  private __paginated__(
    users: UserSchema[],
    count: number,
    page: number
  ): PaginatedDto<UserModelDto> {
    const pages = Math.ceil(count / LIMIT)
    const list: UserModelDto[] = users.map(u => new UserModelDto(u))

    return { count, list, page, pages }
  }

  private async __get_all__(page: number = 1): ResponseSearch<UserModelDto> {
    const skip = (page - 1) * LIMIT
    const take = LIMIT

    const [users, count] = await UserSchema.findAndCount({ skip, take })
    const payload = this.__paginated__(users, count, page)

    return {
      code: 200,
      isError: false,
      message: 'get all users',
      payload,
    }
  }

  private async __get_bookings__(userId: number): Promise<BookingSchema[]> {
    try {
      return await BookingSchema.findBy({ userId })
    } catch (error) {
      console.log(error)
      return []
    }
  }

  async add(userDto: UserDto): Response {
    try {
      const { username, password } = userDto

      const user = new UserSchema()
      user.username = username
      user.password = password
      await user.save()

      return { code: 200, isError: false, message: 'new user' }
    } catch (e) {
      return { code: 500, isError: true, message: (e as Error).message }
    }
  }

  async del(id: number): Response {
    try {
      const { affected } = await UserSchema.delete({ id })
      const bookings = await this.__get_bookings__(id)

      for (const booking of bookings) {
        await booking.remove()
      }

      if (!isDefined(affected) || affected === 0)
        return { code: 400, isError: true, message: 'fail delete user' }

      return { code: 200, isError: false, message: `delete user with id:${id}` }
    } catch (error) {
      return { code: 500, isError: true, message: (error as Error).message }
    }
  }

  async update(e: UserModelDto): Response {
    try {
      const user = await UserSchema.findOneBy({ id: e.id })

      if (user == null)
        return { code: 400, isError: true, message: 'user not found' }

      const result = await UserSchema.update(e.id, e)
      if (result.affected === 0 || result.affected === null)
        return { code: 400, isError: true, message: 'fail updated user' }

      return { code: 200, isError: false, message: 'updating user' }
    } catch (error) {
      return { code: 500, isError: true, message: (error as Error).message }
    }
  }

  async search(
    where: Partial<UserModelDto>,
    page: number = 1
  ): ResponseSearch<UserModelDto> {
    const skip = (page - 1) * LIMIT
    const take = LIMIT

    try {
      if (!isDefined(where) || Object.keys(where).length === 0)
        return await this.__get_all__(page)

      const [users, count] = await UserSchema.findAndCount({
        where,
        skip,
        take,
      })

      const code = 200
      const isError = false
      const message = count === 0 ? 'uses not found' : 'get users'
      const payload = this.__paginated__(users, count, page)

      return { code, isError, payload, message }
    } catch (error) {
      const code = 500
      const isError = true
      const message = 'fail search users'

      return { code, isError, message }
    }
  }
}
