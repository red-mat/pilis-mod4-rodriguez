import type { IUserModelDto } from '@/Users/core/dto'
import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from 'typeorm'

@Entity()
export default class UserSchema extends BaseEntity implements IUserModelDto {
  @PrimaryGeneratedColumn()
  id!: number

  @Column({ unique: true })
  username!: string

  @Column()
  password!: string
}
