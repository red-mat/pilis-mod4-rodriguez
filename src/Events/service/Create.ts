import { type Status } from '@/shared/status'
import { type IEventDto } from '../core/dto/Event'
import { Event, type EventRepository } from '../core/models'
import { isInstance } from '@/shared/validators'

type ErrorResult = Status<undefined>
type Result = Promise<Status<undefined> | ErrorResult>

export default class Create {
  constructor(private readonly repository: EventRepository) {}

  private __validate_event__(eventDto: IEventDto): Event | ErrorResult {
    try {
      return new Event(eventDto)
    } catch (error) {
      console.log((error as Error).message)
      return { isError: true, code: 400, message: 'event data not valid' }
    }
  }

  async run(eventDto: IEventDto): Result {
    const eventResult = this.__validate_event__(eventDto)
    if (!isInstance(eventResult, Event)) return eventResult as ErrorResult

    return await this.repository.add((eventResult as Event).toDto())
  }
}
