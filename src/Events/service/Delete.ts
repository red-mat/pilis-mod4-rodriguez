import { type Status } from '@/shared/status'
import { type EventRepository } from '../core/models'
import { isInt } from '@/shared/validators'

type ErrorResult = Status<undefined>
type Result = Promise<Status<undefined> | ErrorResult>

export default class Create {
  constructor(private readonly repository: EventRepository) {}

  private __get_id__(id: number): Status<number> {
    if (!isInt(id))
      return { code: 400, isError: true, message: 'id not a int number' }

    return { code: 200, isError: false, payload: Number(id) }
  }

  async run(id: number): Result {
    const resultId = this.__get_id__(id)
    if (resultId.isError) return { ...resultId, payload: undefined }

    return await this.repository.del(id)
  }
}
