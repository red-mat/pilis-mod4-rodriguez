/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { type Status } from '@/shared/status'
import { type IEventModelDto, type EventModelDto } from '../core/dto'
import { Event, type EventRepository } from '../core/models'
import { isInt } from '@/shared/validators'
import { type EventDto } from '../core/dto/Event'

type Result = Promise<Status<undefined>>

export default class Update {
  constructor(private readonly repository: EventRepository) {}

  private async __get_event__(id?: number): Promise<Status<EventModelDto>> {
    if (!isInt(id!))
      return { isError: true, code: 400, message: 'id undefined' }

    const result = await this.repository.search({ id })
    if (result.isError)
      return { isError: true, code: result.code, message: result.message }

    const { count, list } = result.payload!
    if (count === 0)
      return { code: 400, isError: true, message: 'event not found' }

    return { code: 200, isError: false, payload: list[0] }
  }

  private __update__(
    eventModel: EventModelDto,
    data: Partial<EventDto>
  ): Status<EventModelDto> {
    const event = new Event(eventModel)

    try {
      event.update(data)
      return {
        code: 200,
        isError: false,
        payload: event.toEventModelDto(eventModel.id),
      }
    } catch (error) {
      return { code: 400, isError: true, message: (error as Error).message }
    }
  }

  async run(eventData: Partial<IEventModelDto>, page = 1): Result {
    const { id, ...updateData } = eventData

    const resultGetEventModel = await this.__get_event__(id)
    if (resultGetEventModel.isError)
      return { ...resultGetEventModel, payload: undefined }

    const eventModel = resultGetEventModel.payload

    const resultUpdate = this.__update__(eventModel!, updateData)
    if (resultUpdate.isError) return { ...resultUpdate, payload: undefined }

    const eventUpdated = resultUpdate.payload!
    return await this.repository.update(eventUpdated)
  }
}
