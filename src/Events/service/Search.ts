import { type PaginatedDto } from '@/shared/paginated'
import { type Status } from '@/shared/status'
import { type IEventModelDto, type EventModelDto } from '../core/dto'
import { type EventRepository } from '../core/models'

type ResultEvent = Status<PaginatedDto<EventModelDto>>
type Result = Promise<ResultEvent>

export default class Search {
  constructor(private readonly repository: EventRepository) {}

  async run(eventDto: Partial<IEventModelDto>, page = 1): Result {
    return await this.repository.search(eventDto, page)
  }
}
