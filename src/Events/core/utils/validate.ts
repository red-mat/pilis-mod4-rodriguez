import { isInt, isNumber, isString } from '@/shared/validators'

export const isId = (id: any): boolean => isNumber(id)
export const isName = (name: any): boolean => isString(name)
export const isDescription = (desc: any): boolean => isString(desc)
export const isPlace = (place: any): boolean => isString(place)
export const isDateTime = (time: any): boolean => !isNaN(new Date(time) as any)
export const isPrice = (price: any): boolean => isNumber(price) && price >= 0
export const isLimit = (limit: any): boolean => isInt(limit) && limit > 0
export const isTypeEvent = (typeEvent: any): boolean => isString(typeEvent)
