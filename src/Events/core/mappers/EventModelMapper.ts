import { EventModelDto } from '../dto'
import { EventDto } from '../dto/Event'

export default class EventMapper {
  constructor(private readonly dto: EventModelDto) {}

  toDto(): EventModelDto {
    const dto = structuredClone(this.dto)
    return new EventModelDto(dto)
  }

  toEventDto(id: number): EventDto {
    const dto = structuredClone(this.dto)
    return new EventDto(dto)
  }
}
