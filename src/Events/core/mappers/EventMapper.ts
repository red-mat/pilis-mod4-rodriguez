import { EventModelDto } from '../dto'
import { EventDto } from '../dto/Event'

export default class EventMapper {
  constructor(private readonly dto: EventDto) {}

  toDto(): EventDto {
    const dto = structuredClone(this.dto)
    return new EventDto(dto)
  }

  toEventModelDto(id: number): EventModelDto {
    const dto = structuredClone(this.dto)
    const model = { ...dto, id }
    return new EventModelDto(model)
  }
}
