import { Repository } from '@/shared/repository'
import { type EventModelDto } from '../dto'
import { type EventDto } from '../dto/Event'

export default abstract class EventRepository extends Repository<
  EventModelDto,
  EventDto,
  number
> {}
