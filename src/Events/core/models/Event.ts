/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { isDefined } from '@/shared/validators'
import { type EventModelDto, type GpsDto, type IGpsDto } from '../dto'
import { EventDto, type IEventDto } from '../dto/Event'
import { EventMapper } from '../mappers'
import {
  isDateTime,
  isDescription,
  isLimit,
  isName,
  isPlace,
  isPrice,
  isTypeEvent,
} from '../utils/validate'
import Gps from './Gps'

export default class Event {
  private mapper: EventMapper

  constructor(private dto: IEventDto) {
    this.dto = this.__verify__(dto)
    this.mapper = new EventMapper(this.dto)
  }

  private __verify__(dto: IEventDto): EventDto {
    const event: IEventDto = {
      name: this.__check_name__(dto.name),
      description: this.__check_description__(dto.description),
      place: this.__check_place__(dto.place),
      dateTime: this.__check_time__(dto.dateTime),
      gps: this.__check_gps__(dto.gps),
      price: this.__check_price__(dto.price),
      limit: this.__check_limit__(dto.limit),
      typeEvent: this.__check_typeEvent__(dto.typeEvent),
    }
    return new EventDto(event)
  }

  private __check_name__(name: string): string {
    if (!isName(name)) throw new Error('name not valid')
    return name
  }

  private __check_description__(desc: string): string {
    if (!isDescription(desc)) throw new Error('description not valid')
    return desc
  }

  private __check_place__(place: string): string {
    if (!isPlace(place)) throw new Error('place not valid')
    return place
  }

  private __check_time__(time: string): string {
    if (!isDateTime(time)) throw new Error('time string not valid')
    return time
  }

  private __check_gps__(gps: IGpsDto): GpsDto {
    const gpsModel = new Gps(gps)
    return gpsModel.toDto()
  }

  private __check_price__(price: number): number {
    if (!isPrice(price)) throw new Error('price not valid')
    return price
  }

  private __check_limit__(limit: number): number {
    if (!isLimit(limit)) throw new Error('limit number not valid')
    return limit
  }

  private __check_typeEvent__(typeEvent: string): string {
    if (!isTypeEvent(typeEvent)) throw new Error('type event not valid')
    return typeEvent
  }

  toDto(): EventDto {
    return this.mapper.toDto()
  }

  toEventModelDto(id: number): EventModelDto {
    return this.mapper.toEventModelDto(id)
  }

  update(e: Partial<IEventDto>): void {
    const dto = this.toDto()
    const event: IEventDto = {
      name: isDefined(e.name) ? this.__check_name__(e.name!) : dto.name,
      description: isDefined(e.description)
        ? this.__check_description__(e.description!)
        : dto.description,
      place: isDefined(e.place) ? this.__check_place__(e.place!) : dto.place,
      dateTime: isDefined(e.dateTime)
        ? this.__check_time__(e.dateTime!)
        : dto.dateTime,
      gps: isDefined(e.gps) ? this.__check_gps__(e.gps!) : dto.gps,
      price: isDefined(e.price) ? this.__check_price__(e.price!) : dto.price,
      limit: isDefined(e.limit) ? this.__check_limit__(e.limit!) : dto.limit,
      typeEvent: isDefined(e.typeEvent)
        ? this.__check_typeEvent__(e.typeEvent!)
        : dto.typeEvent,
    }
    const eventDto = new EventDto(event)
    const mapper = new EventMapper(eventDto)

    this.dto = eventDto
    this.mapper = mapper
  }
}
