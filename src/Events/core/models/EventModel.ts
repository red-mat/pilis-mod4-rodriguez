import { EventModelDto, type IEventModelDto } from '../dto'
import { type EventDto } from '../dto/Event'
import { EventMapper } from '../mappers'

export default class Event {
  private readonly mapper: EventMapper

  constructor(dto: IEventModelDto) {
    const eventModelDto = new EventModelDto(dto)
    this.mapper = new EventMapper(eventModelDto)
  }

  toDto(): EventDto {
    return this.mapper.toDto()
  }

  toEventModelDto(id: number): EventModelDto {
    return this.mapper.toEventModelDto(id)
  }
}
