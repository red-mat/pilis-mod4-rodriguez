import { isNumber } from '@/shared/validators'
import { GpsDto, type IGpsDto } from '../dto'

export default class Gps {
  constructor(private readonly dto: IGpsDto) {
    this.dto = this.__verify__(dto)
  }

  private __verify__(dto: IGpsDto): GpsDto {
    const { latitude, longitude } = dto

    const gps = {
      latitude: this.__check_latitude___(latitude),
      longitude: this.__check_longitude___(longitude),
    }

    return new GpsDto(gps)
  }

  private __check_latitude___(latitude: number): number {
    if (!isNumber(latitude)) throw new Error('latitude value not valid')
    if (latitude < -90 || latitude > 90)
      throw new Error('latitude value not valid')

    return latitude
  }

  private __check_longitude___(longitude: number): number {
    if (!isNumber(longitude)) throw new Error('longitude value not valid')
    if (longitude < -180 || longitude > 180)
      throw new Error('longitude value not valid')

    return longitude
  }

  toDto(): GpsDto {
    const dto = structuredClone(this.dto)
    return new GpsDto(dto)
  }
}
