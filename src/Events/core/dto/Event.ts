import type { GpsDto } from './Gps'

export interface IEventDto {
  name: string
  description: string
  place: string
  dateTime: string
  gps: GpsDto
  price: number
  limit: number
  typeEvent: string
}

export class EventDto implements IEventDto {
  readonly name: string
  readonly description: string
  readonly place: string
  readonly dateTime: string
  readonly gps: GpsDto
  readonly price: number
  readonly limit: number
  readonly typeEvent: string

  constructor(dto: IEventDto) {
    this.name = dto.name
    this.description = dto.description
    this.place = dto.place
    this.dateTime = dto.dateTime
    this.gps = dto.gps
    this.price = dto.price
    this.limit = dto.limit
    this.typeEvent = dto.typeEvent
  }
}
