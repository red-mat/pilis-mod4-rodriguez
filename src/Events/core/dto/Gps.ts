export interface IGpsDto {
  latitude: number
  longitude: number
}

export class GpsDto implements IGpsDto {
  readonly latitude: number
  readonly longitude: number

  constructor(dto: IGpsDto) {
    this.latitude = dto.latitude
    this.longitude = dto.longitude
  }
}
