import auth from '@/Users/infrastructure/middleware/auth'
import { Router } from 'express'
import { Create, Delete, Search, Update } from './controllers'

const PATH = '/events'

const route = Router()

route.use(PATH, auth)

const searchController = new Search(PATH)
route.get(searchController.path, searchController.handler)

const createController = new Create(PATH)
route.post(createController.path, createController.handler)

const updateController = new Update(PATH)
route.put(updateController.path, updateController.handler)

const deleteController = new Delete(PATH)
route.delete(deleteController.withParam('id'), deleteController.handler)

export default route
