import { Search } from '@/Events/service'
import { Controller } from '@/shared/controller'
import type { Request, Response } from 'express'
import EventsMySqlRepository from '../../../EventsMySqlRepository'
import { type EventDto } from '@/Events/core/dto/Event'
import {
  type PaginatedDto,
  PaginatedMapper,
  type PaginatedResponse,
} from '@/shared/paginated'
import { isDefined } from '@/shared/validators'

export default class SearchController extends Controller {
  private readonly service: Search
  constructor(path: string) {
    super(path)
    const repository = new EventsMySqlRepository()
    this.service = new Search(repository)
  }

  private __paginated__(
    paginated: PaginatedDto<EventDto>,
    url: string
  ): PaginatedResponse<EventDto> {
    const mapper = new PaginatedMapper(url)
    return mapper.toPaginatedResponse(paginated)
  }

  private __get_url__(req: Request): string {
    const protocol = req.protocol
    const host = req.get('host') as string
    const root = req.baseUrl

    return protocol + '://' + host + root + '/users'
  }

  handler(req: Request, res: Response): void {
    const params = req.body
    this.service
      .run(params)
      .then(result => {
        if (!isDefined(result.payload)) res.status(result.code).send(result)
        else {
          const url = this.__get_url__(req)
          // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
          const payload = this.__paginated__(result.payload!, url)
          res.status(result.code).send({ ...result, payload })
        }
      })
      .catch((e: Error) => {
        console.log(e.message)
        res.sendStatus(500)
      })
  }
}
