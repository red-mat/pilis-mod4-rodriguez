import { Controller } from '@/shared/controller'
import type { Request, Response } from 'express'
import EventsMySqlRepository from '../../../EventsMySqlRepository'
import { Delete } from '@/Events/service'

export default class DeleteController extends Controller {
  private readonly service: Delete
  constructor(path: string) {
    super(path)
    const repository = new EventsMySqlRepository()
    this.service = new Delete(repository)
  }

  handler(req: Request, res: Response): void {
    const id = req.params.id
    this.service
      .run(Number(id))
      .then(status => res.status(status.code).send(status))
      .catch((e: Error) => {
        console.log(e.message)
        res.sendStatus(500)
      })
  }
}
