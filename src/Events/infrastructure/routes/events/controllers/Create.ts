import Create from '@/Events/service/Create'
import { Controller } from '@/shared/controller'
import type { Request, Response } from 'express'
import EventsMySqlRepository from '../../../EventsMySqlRepository'

export default class CreateController extends Controller {
  private readonly service: Create
  constructor(path: string) {
    super(path)
    const repository = new EventsMySqlRepository()
    this.service = new Create(repository)
  }

  handler(req: Request, res: Response): void {
    const data = req.body
    this.service
      .run(data)
      .then(status => res.status(status.code).send(status))
      .catch((e: Error) => {
        console.log(e.message)
        res.sendStatus(500)
      })
  }
}
