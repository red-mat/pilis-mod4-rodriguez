import { Update } from '@/Events/service'
import { Controller } from '@/shared/controller'
import type { Request, Response } from 'express'
import EventsMySqlRepository from '../../../EventsMySqlRepository'

export default class UpdateController extends Controller {
  private readonly service: Update
  constructor(path: string) {
    super(path)
    const repository = new EventsMySqlRepository()
    this.service = new Update(repository)
  }

  handler(req: Request, res: Response): void {
    const params = req.body
    this.service
      .run(params)
      .then(status => res.status(status.code).send(status))
      .catch((e: Error) => {
        console.log(e.message)
        res.sendStatus(500)
      })
  }
}
