import { type IEventModelDto } from '@/Events/core/dto'
import {
  BaseEntity,
  Column,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm'

@Entity()
export class GpsSchema extends BaseEntity {
  @PrimaryGeneratedColumn()
  id!: number

  @Column({ type: 'double' })
  latitude!: number

  @Column({ type: 'double' })
  longitude!: number
}

@Entity()
export class EventsSchema extends BaseEntity implements IEventModelDto {
  @PrimaryGeneratedColumn()
  id!: number

  @Column()
  name!: string

  @Column()
  description!: string

  @Column()
  place!: string

  @Column({ type: 'datetime' })
  dateTime!: string

  @OneToOne(() => GpsSchema, { eager: true })
  @JoinColumn()
  gps!: GpsSchema

  @Column({ type: 'float' })
  price!: number

  @Column({ type: 'int' })
  limit!: number

  @Column()
  typeEvent!: string
}
