/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { EventModelDto, type GpsDto } from '@/Events/core/dto'
import { type EventDto } from '@/Events/core/dto/Event'
import { EventRepository } from '@/Events/core/models'
import { type PaginatedDto } from '@/shared/paginated'
import { type Response, type ResponseSearch } from '@/shared/repository'
import { isDefined } from '@/shared/validators'
import { EventsSchema, GpsSchema } from './schema'
import { BookingSchema } from '@/Bookings/entity'

const LIMIT = 20
export default class EventsMySqlRepository extends EventRepository {
  private __get_gps_scheme(g: GpsDto): GpsSchema {
    const gps = new GpsSchema()
    gps.latitude = g.latitude
    gps.longitude = g.longitude
    return gps
  }

  private async __get_event_scheme__(e: EventDto): Promise<EventsSchema> {
    const gps = this.__get_gps_scheme(e.gps)
    await gps.save()

    const event = new EventsSchema()
    event.name = e.name
    event.description = e.description
    event.place = e.place
    event.dateTime = e.dateTime
    event.gps = gps
    event.price = e.price
    event.limit = e.limit
    event.typeEvent = e.typeEvent
    return event
  }

  private __paginated__(
    events: EventsSchema[],
    count: number,
    page: number
  ): PaginatedDto<EventModelDto> {
    const pages = Math.ceil(count / LIMIT)
    const list: EventModelDto[] = events
      .map(e => new EventModelDto(e))
      .map(e => ({
        ...e,
        gps: { latitude: e.gps.latitude, longitude: e.gps.longitude },
      }))

    return { count, list, page, pages }
  }

  private async __get_all__(page: number = 1): ResponseSearch<EventModelDto> {
    const skip = (page - 1) * LIMIT
    const take = LIMIT

    const [events, count] = await EventsSchema.findAndCount({
      skip,
      take,
      relations: { gps: true },
    })
    const payload = this.__paginated__(events, count, page)

    return {
      code: 200,
      isError: false,
      message: 'get all events',
      payload,
    }
  }

  private async __find__(
    params: Partial<EventModelDto>,
    page: number
  ): Promise<{ events: EventsSchema[]; count: number }> {
    const skip = (page - 1) * LIMIT
    const take = LIMIT

    const where = params
    const [events, count] = await EventsSchema.findAndCount({
      where,
      skip,
      take,
    })

    return { events, count }
  }

  private async __get_bookings__(eventId: number): Promise<BookingSchema[]> {
    try {
      return await BookingSchema.findBy({ eventId })
    } catch (error) {
      console.log(error)
      return []
    }
  }

  async add(e: EventDto): Response {
    try {
      const event = await this.__get_event_scheme__(e)
      await event.save()
      return {
        code: 200,
        isError: false,
        message: `create event with id is ${event.id}`,
      }
    } catch (error) {
      console.log((error as Error).message)

      return {
        code: 500,
        isError: true,
        message: 'fail create event',
      }
    }
  }

  async del(id: number): Response {
    const event = await EventsSchema.findOne({ where: { id } })
    const gps = await GpsSchema.findOne({ where: { id: event?.gps.id } })
    const bookings = await this.__get_bookings__(id)

    try {
      if (!isDefined(event))
        return { code: 400, isError: true, message: 'fail delete events' }

      await event?.remove()
      await gps?.remove()
      for (const booking of bookings) {
        await booking.remove()
      }

      return { code: 200, isError: false, message: `delete user with id:${id}` }
    } catch (error) {
      return { code: 500, isError: true, message: (error as Error).message }
    }
  }

  async update(e: EventModelDto): Response {
    try {
      const { gps, ...updated } = e
      const event = await EventsSchema.findOneBy({ id: e.id })

      if (!isDefined(event))
        return { code: 400, isError: true, message: 'event not found' }

      await GpsSchema.update(event!.gps.id, gps)
      const { affected } = await EventsSchema.update(e.id, updated)

      if (!isDefined(affected) || affected === 0)
        return { code: 400, isError: true, message: 'event data not valid' }

      return {
        code: 200,
        isError: false,
        message: `update event with id: ${e.id}`,
      }
    } catch (error) {
      console.log((error as Error).message)
      return { code: 500, isError: true, message: 'fail update event' }
    }
  }

  async search(
    where: Partial<EventModelDto>,
    page = 1
  ): ResponseSearch<EventModelDto> {
    try {
      if (!isDefined(where) || Object.keys(where).length === 0)
        return await this.__get_all__(page)

      const { events, count } = await this.__find__(where, page)

      return {
        code: 200,
        isError: false,
        message: count === 0 ? 'event not found' : 'get events',
        payload: this.__paginated__(events, count, page),
      }
    } catch (error) {
      console.log(error)
      return {
        code: 500,
        isError: true,
        message: 'fail search events',
      }
    }
  }
}
